variable "name" {
  type = "string"
}

variable "instance_count" {
  type = "string"
}

variable "ignition" {
  type    = "string"
  default = ""
}

variable "ignition_url" {
  type    = "string"
  default = ""
}

variable "catalog" {
  type = "string"
}

variable "template" {
  type = "string"
}

variable "network" {
  type = "string"
}

variable "cluster_domain" {
  type = "string"
}

variable "ip_addresses" {
  type = "list"
}

variable "cpus" {
  type = "string"
}

variable "memory" {
  type = "string"
}