resource "vcd_vapp_vm" "node" {
  count            = "${var.instance_count}"

  vapp_name        = "${var.name}-${count.index}"
  name             = "${var.name}-${count.index}"
  catalog_name     = "${var.catalog}"
  template_name    = "${var.template}"
  cpus             = "${var.cpus}"
  memory           = "${var.memory}"
  cpu_cores        = 1

  network {
    type               = "org"
    name               = "${var.network}"
    ip_allocation_mode = "DHCP"
  }

  metadata = {
    role           = "${var.name}"
    cluster_domain = "${var.cluster_domain}"
  }
  
  depends_on = ["${var.name}-${count.index}"]
}
