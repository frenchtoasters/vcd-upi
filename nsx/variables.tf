variable "infra_ips" {
  type = "list"
}

variable "control_ips" {
  type = "list"
}

variable "org" {
  type = "string"
}

variable "vdc" {
  type = "string"
}

variable "edge_gateway" {
  type = "string"
}

variable "name" {
  type = "string"
}

variable "edge_ip" {
  type = "string"
}

variable "bootstrap_complete" {
  type    = string
  default = "false"
}

variable "bootstrap_ip" {
  type    = list(string)
  default = []
}