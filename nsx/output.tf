output "api-server" {
  value = vcd_lb_virtual_server.api-server.id
}

output "api-int-server" {
  value = vcd_lb_virtual_server.api-int-server.id
}

output "http" {
  value = vcd_lb_virtual_server.http.id
}

output "https" {
  value = vcd_lb_virtual_server.https.id
}