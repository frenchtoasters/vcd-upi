locals {
  control = {
    for ip in var.control_ips:
    replace(ip,".","-") => ip
  }
  infra_ip = {
    for ip in var.infra_ips:
    replace(ip,".","-") => ip
  }
  bootstrap = {
    for ip in var.bootstrap_ip:
    replace(ip, ".", "-") => ip
  }
}

resource "vcd_lb_server_pool" "control-int-pool" {
  org          = var.org
  vdc          = var.vdc
  edge_gateway = var.edge_gateway

  name      = "control-int-pool"
  algorithm = "round-robin"

  dynamic "member" {
    for_each = merge(local.control, local.bootstrap)

    content {
      condition       = "enabled"
      name            = "control_${member.key}"
      ip_address      = member.value
      port            = 22623
      monitor_port    = 22623
      weight          = 1
    }
  }
}

resource "vcd_lb_server_pool" "control-api-pool" {
  org          = var.org
  vdc          = var.vdc
  edge_gateway = var.edge_gateway

  name      = "control-api-pool"
  algorithm = "round-robin"

  dynamic "member" {
    for_each = merge(local.control, local.bootstrap)

    content {
      condition       = "enabled"
      name            = "control_${member.key}"
      ip_address      = member.value
      port            = 6443
      monitor_port    = 6443
      weight          = 1
    }
  }
}

resource "vcd_lb_server_pool" "infra-http-pool" {
  org          = var.org
  vdc          = var.vdc
  edge_gateway = var.edge_gateway

  name      = "infra-http-pool"
  algorithm = "round-robin"

  dynamic "member" {
    for_each = local.infra_ip

    content {
      condition       = "enabled"
      name            = "infra_${member.key}"
      ip_address      = member.value
      port            = 80
      monitor_port    = 80
      weight          = 1
    }
  }
}

resource "vcd_lb_server_pool" "infra-https-pool" {
  org          = var.org
  vdc          = var.vdc
  edge_gateway = var.edge_gateway

  name      = "infra-https-pool"
  algorithm = "round-robin"

  dynamic "member" {
    for_each = local.infra_ip

    content {
      condition       = "enabled"
      name            = "infra_${member.key}"
      ip_address      = member.value
      port            = 443
      monitor_port    = 443
      weight          = 1
    }
  }
}

resource "vcd_lb_virtual_server" "api-server" {
  org = var.org
  vdc = var.vdc
  edge_gateway = var.edge_gateway

  name = "k8-api-server"
  description = "api.cluster_name.domain"
  ip_address = var.edge_ip
  protocol = "tcp"
  port = "6443"

  server_pool_id = vcd_lb_server_pool.control-api-pool.id
}

resource "vcd_lb_virtual_server" "api-int-server" {
  org = var.org
  vdc = var.vdc
  edge_gateway = var.edge_gateway

  name = "k8-api-int-server"
  description = "api-int.cluster_name.domain"
  ip_address = var.edge_ip
  protocol = "tcp"
  port = "22623"

  server_pool_id = vcd_lb_server_pool.control-int-pool.id
}

resource "vcd_lb_virtual_server" "http" {
  org = var.org
  vdc = var.vdc
  edge_gateway = var.edge_gateway

  name = "infra-http"
  description = "*.apps.cluster_name.domain"
  ip_address = var.edge_ip
  protocol = "tcp"
  port = "80"

  server_pool_id = vcd_lb_server_pool.infra-http-pool.id
}

resource "vcd_lb_virtual_server" "https" {
  org = var.org
  vdc = var.vdc
  edge_gateway = var.edge_gateway

  name = "infra-https"
  description = "*.apps.cluster_name.domain"
  ip_address = var.edge_ip
  protocol = "tcp"
  port = "443"

  server_pool_id = vcd_lb_server_pool.infra-https-pool.id
}