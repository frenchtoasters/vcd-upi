//////
// vCD variables
//////

variable "vcd_url" {
  type        = string
  description = "This is the url for the vCD environment."
}

variable "vcd_user" {
  type        = string
  description = "vCD server user for the environment."
}

variable "vcd_password" {
  type        = string
  description = "vCD server password"
}

variable "vcd_org" {
  type        = string
  description = "This is the name of the vCD org."
}

variable "vcd_vdc" {
  type        = string
  description = "This is the name of the vCD Virtual Data Center."
}

variable "catalog_name" {
  type        = string
  description = "This is the name of the catalog."
}

variable "template_name" {
  type        = string
  description = "This is the name of the VAPP template to clone."
}

variable "vapp_network" {
  type        = string
  description = "This is the name of the publicly accessible network for cluster ingress and access."
  default     = "VM Network"
}

variable "ipam" {
  type        = string
  description = "The IPAM server to use for IP management."
  default     = ""
}

variable "ipam_token" {
  type        = string
  description = "The IPAM token to use for requests."
  default     = ""
}

variable "vcd_edge" {
  type        = string
  description = "The vCD edge name to use for deployment."
  default     = ""
}

variable "vcd_lb_name" {
  type        = string
  description = "The vCD loadbalancer name to use for deployment."
  default     = ""
}

variable "vcd_edge_ip" {
  type        = string
  description = "The external IP of the NSX Edge."
  default     = ""
}

/////////
// vSphere variables
////////

variable "vsphere_server" {
  type        = string
  description = "This is the vSphere server for the environment."
}

variable "vsphere_user" {
  type        = string
  description = "vSphere server user for the environment."
}

variable "vsphere_password" {
  type        = string
  description = "vSphere server password"
}

variable "vsphere_cluster" {
  type        = string
  description = "This is the name of the vSphere cluster."
}

variable "vsphere_datacenter" {
  type        = string
  description = "This is the name of the vSphere data center."
}

variable "vsphere_datastore" {
  type        = string
  description = "This is the name of the vSphere data store."
}

variable "vm_template" {
  type        = string
  description = "This is the name of the VM template to clone."
}

variable "vm_network" {
  type        = string
  description = "This is the name of the publicly accessible network for cluster ingress and access."
  default     = "VM Network"
}

/////////
// AWS variables
////////

variable "aws_region" {
  type        = string
  description = "This is the aws region to deploy things to."
  default     = "us-east-1"
}

variable "aws_access_key" {
  type        = string
  description = "This is the aws access key."
}

variable "aws_secret_key" {
  type        = string
  description = "This is the aws secret key."
}

/////////
// OpenShift cluster variables
/////////

variable "cluster_id" {
  type        = string
  description = "This cluster id must be of max length 27 and must have only alphanumeric or hyphen characters."
}

variable "base_domain" {
  type        = string
  description = "The base DNS zone to add the sub zone to."
}

variable "cluster_domain" {
  type        = string
  description = "The base DNS zone to add the sub zone to."
}

variable "machine_cidr" {
  type = string
}

/////////
// Bootstrap machine variables
/////////

variable "bootstrap_complete" {
  type    = string
  default = "false"
}

variable "bootstrap_ignition_url" {
  type = string
}

variable "bootstrap_ip" {
  type    = list(string)
  default = []
}

///////////
// Control Plane machine variables
///////////

variable "control_plane_count" {
  type    = string
  default = "3"
}

variable "control_plane_ignition" {
  type = string
}

variable "control_plane_ips" {
  type    = list(string)
  default = []
}

//////////
// Infra machine variables
/////////

variable "infra_count" {
  type    = string
  default = "2"
}

variable "infra_ips" {
  type    = list(string)
  default = []
}

//////////
// Compute machine variables
//////////

variable "compute_count" {
  type    = string
  default = "3"
}

variable "compute_ignition" {
  type = string
}

variable "compute_ips" {
  type    = list(string)
  default = []
}

variable "compute_cpus" {
  type    = string
  default = "2"
}

variable "compute_memory" {
  type    = string
  default = "4096"
}

//////////
// Switch for more consistent deployment
//////////

variable "deployed" {
  type    = string
  default = "false"
}

