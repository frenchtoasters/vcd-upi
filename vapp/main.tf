resource "vcd_vapp" "node" {
  count            = "${var.instance_count}"

  name             = "${var.name}-${count.index}"
  catalog_name     = "${var.catalog}"
  template_name    = "${var.template}"
  cpus             = "${var.cpus}"
  memory           = "${var.memory}"

  // Switch needed for consistency in deployment
  power_on         = "${var.power_state}"


  network_name = "${var.network}"

  metadata = {
    role           = "${var.name}"
    cluster_domain = "${var.cluster_domain}"
  }

  ovf = {
    "guestinfo.ignition.config.data"          = "${base64encode(data.ignition_config.ign.*.rendered[count.index])}"
    "guestinfo.ignition.config.data.encoding" = "base64"
  }
}