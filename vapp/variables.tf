variable "instance_count" {
  type = "string"
}

variable "name" {
  type = "string"
}

variable "ignition_url" {
  type    = "string"
  default = ""
}

variable "ignition" {
  type    = "string"
  default = ""
}

variable "cluster_domain" {
  type = "string"
}

variable "machine_cidr" {
  type = "string"
}

variable "ipam" {
  type = "string"
}

variable "ipam_token" {
  type = "string"
}

variable "ip_addresses" {
  type = "list"
}

variable "catalog" {
  type = "string"
}

variable "template" {
  type = "string"
}

variable "network" {
  type = "string"
}

variable "power_state" {
  type = "string"
}

variable "cpus" {
  type    = "string"
  default = "2"
}

variable "memory" {
  type    = "string"
  default = "4096"
}