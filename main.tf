provider "aws" {
  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

provider "vcd" {
  url                  = var.vcd_url
  user                 = var.vcd_user
  password             = var.vcd_password

  sysorg               = "System"
  org                  = var.vcd_org
  vdc                  = var.vcd_vdc
  allow_unverified_ssl = true
}

provider "vsphere" {
  user                 = "${var.vsphere_user}"
  password             = "${var.vsphere_password}"
  vsphere_server       = "${var.vsphere_server}"
  allow_unverified_ssl = true
}

data "vsphere_datacenter" "dc" {
  name = "${var.vsphere_datacenter}"
}

module "folder" {
  source        = "./folder"

  path          = "${var.cluster_id}"
  datacenter_id = "${data.vsphere_datacenter.dc.id}"
}

module "resource_pool" {
  source          = "./resource_pool"

  name            = "${var.cluster_id}"
  datacenter_id   = "${data.vsphere_datacenter.dc.id}"
  vsphere_cluster = "${var.vsphere_cluster}"
}

module "bootstrap_vm" {
  source           = "./virtualmachine"
  //source           = "./vapp"

  name             = "bootstrap"

  //power_state      = var.deployed ? true : false

  instance_count   = var.bootstrap_complete ? 0 : 1
  ignition_url     = var.bootstrap_ignition_url

  resource_pool_id = module.resource_pool.pool_id
  datastore        = var.vsphere_datastore
  folder           = module.folder.path
  network          = var.vm_network
  datacenter_id    = data.vsphere_datacenter.dc.id
  template         = var.vm_template

  //ipam             = var.ipam
  //ipam_token       = var.ipam_token
  cluster_domain   = var.cluster_domain
  //catalog          = var.catalog_name
  //template         = var.template_name
  //network          = var.vapp_network
  ip_addresses     = var.bootstrap_ip
  machine_cidr     = var.machine_cidr
}

module "control_plane_vms" {
  source           = "./virtualmachine"
  //source           = "./vapp"

  name             = "control-plane"

  //power_state      = var.deployed ? true : false

  instance_count   = var.control_plane_count
  ignition         = var.control_plane_ignition

  resource_pool_id = module.resource_pool.pool_id
  datastore        = var.vsphere_datastore
  folder           = module.folder.path
  network          = var.vm_network
  datacenter_id    = data.vsphere_datacenter.dc.id
  template         = var.vm_template
  
  //ipam             = var.ipam
  //ipam_token       = var.ipam_token
  cluster_domain   = var.cluster_domain
  //catalog          = var.catalog_name
  //template         = var.template_name
  //network          = var.vapp_network
  ip_addresses     = var.control_plane_ips
  machine_cidr     = var.machine_cidr
}

module "infra_vms" {
  source           = "./virtualmachine"
  //source           = "./vapp"

  name             = "infra"

  //power_state      = var.deployed ? true : false

  instance_count   = var.infra_count
  ignition         = var.compute_ignition

  resource_pool_id = module.resource_pool.pool_id
  datastore        = var.vsphere_datastore
  folder           = module.folder.path
  network          = var.vm_network
  datacenter_id    = data.vsphere_datacenter.dc.id
  template         = var.vm_template
  
  //ipam             = var.ipam
  //ipam_token       = var.ipam_token
  cluster_domain   = var.cluster_domain
  //catalog          = var.catalog_name
  //template         = var.template_name
  //network          = var.vapp_network
  ip_addresses     = var.infra_ips
  machine_cidr     = var.machine_cidr
}

module "compute_vapps" {
  source         = "./vapp"

  name           = "compute"
  power_state    = var.deployed ? true : false
  instance_count = var.compute_count
  ignition       = var.compute_ignition
  cluster_domain = var.cluster_domain
  catalog        = var.catalog_name
  template       = var.template_name
  network        = var.vapp_network
  ipam           = var.ipam
  ipam_token     = var.ipam_token
  ip_addresses   = var.compute_ips
  machine_cidr   = var.machine_cidr

  cpus           = var.compute_cpus
  memory         = var.compute_memory
}

module "nsx_loadbalancer" {
  source       = "./nsx"

  org          = var.vcd_org
  vdc          = var.vcd_vdc

  edge_gateway = var.vcd_edge
  name         = var.vcd_lb_name

  edge_ip      = var.vcd_edge_ip
  infra_ips    = var.infra_ips
  control_ips  = var.control_plane_ips

  bootstrap_ip = var.bootstrap_complete ? [] : var.bootstrap_ip
  bootstrap_complete = var.bootstrap_complete ? 0 : 1
}

module "dns" {
  source              = "./dns"

  base_domain         = var.base_domain
  cluster_domain      = var.cluster_domain
  control_plane_count = var.control_plane_count
  control_plane_ips   = var.control_plane_ips
  infra_count         = var.infra_count
  infra_ips           = var.infra_ips
  compute_count       = var.compute_count
  compute_ips         = var.compute_ips

  bootstrap_count     = var.bootstrap_complete ? 0 : 1
  bootstrap_ips       = var.bootstrap_complete ? [] : var.bootstrap_ip
}
