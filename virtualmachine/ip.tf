locals {
  network      = "${cidrhost(var.machine_cidr,0)}"
  ip_addresses = "${var.ip_addresses}"
}
